debug:
	g++ -Wall -Wextra -pedantic -ggdb -o flares.exe flares.cpp -lgdi32
release:
	g++ -Wall -Wextra -pedantic -O5 -o flares.exe flares.cpp -lgdi32
clean:
	rm *.exe
