#include "CImg.h"
#include <algorithm>
#include <cmath>
#include <iostream>

using namespace cimg_library;

void display();
int flare1(const int x,const int y,const int xs,const int ys,const int radius);
int flare2(const int x,const int y,const int xs,const int ys,const int radius);
int flare3(const int x,const int y,const int xs,const int ys,const int radius);
double smoothstep(const double a,const double b,const double x);

int main() {

  
  display();
  
  return 0;
}



void display(){
  int width = 400;
  int height = 300;
  CImg<unsigned char> img(width,height,1,3,0);
  CImgDisplay disp(img,"Flares");
  int i = 0;
  int white = 0;
 
  

  while(!disp.is_closed() && !disp.is_keyQ() && !disp.is_keyESC()) {
    for(int x = 0; x<width; x++){
      for(int y = 0; y<height; y++){
	
	white = 0;
	white += flare1(x,y,10,10,30);
	white += flare2(x,y,50,50,40);
	white += flare1(x,y,130,130,(int)50*std::abs(std::sin(i*0.1)));
	white += flare3(x,y,200,200,50);
	
	white = white>255? 255:white;
	int color[] = {white,white,white};
	img.draw_point(x,y,color);
      }
    }
    
    img.display(disp.wait(50));
    i++;
  }
}

int flare3(const int x,const int y,const int xs,const int ys,const int radius)
{
  if(x < (xs - radius) || x > (xs + radius)) return 0;
  if(y < (ys - radius) || y > (ys + radius)) return 0;
  //  return 255;
  
  double r = radius;
  double dx = xs - x;
  double dy = ys - y;
  double rr = std::sqrt(dx*dx + dy*dy);
  double c = rr/r * rr/r;
  c = c*c;
  c = c*c*c;

  //if(rr > radius) c = 0;
  c *= 1 - smoothstep(1 - 0.01, 1 + 0.01, rr / radius);
  c = c * 255;
  return (int)c;

  /*
// r^6
c = r*r;
c = c*c;
c = c*c*c;
if (r>1) c=0;
*/
  return 0;
}

int flare2(const int x,const int y,const int xs,const int ys,const int radius)
{
  if(x < (xs - radius) || x > (xs + radius)) return 0;
  if(y < (ys - radius) || y > (ys + radius)) return 0;
  //  return 255;
  
  double r = radius;
  double dx = xs - x;
  double dy = ys - y;
  double rr = std::sqrt(dx*dx + dy*dy);
  double c = rr/r;

  //if(rr > radius) c = 0;
  c *= 1 - smoothstep(1 - 0.01, 1 + 0.01, rr / radius);
  c = c * 255;
  return (int)c;
  
}

int flare1(const int x,const int y,const int xs,const int ys,const int radius)
{
  if(x < (xs - radius) || x > (xs + radius)) return 0;
  if(y < (ys - radius) || y > (ys + radius)) return 0;
  //  return 255;
  
  double r = radius;
  double dx = xs - x;
  double dy = ys - y;
  double rr = std::sqrt(dx*dx + dy*dy);
  double c = 1-rr/r;

  c = c * c;
  if(rr > radius) c = 0;
  c = c * 255;
  return (int)c;
  
}

double smoothstep(const double a,const double b,const double x)
{
  if(x < a) return 0;
  if(x >= b) return 1;
  double xx = (x - a) / (b - a);
  return xx * xx * (3 - 2 * xx);
}
